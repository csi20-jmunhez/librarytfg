﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Data;
using ProyectoTFG.Filters;
using ProyectoTFG.Models;

namespace ProyectoTFG.Controllers
{
    [ServiceFilter(typeof(VerificaSesion))]
    [ServiceFilter(typeof(VerificaAdmin))]
    public class BookingsController : Controller
    {
        private readonly MiDbContext _context;

        public BookingsController(MiDbContext context)
        {
            _context = context;
        }

        // GET: Bookings
        //public async Task<IActionResult> Index()
        //{
        //    var miDbContext = _context.Bookings.Include(b => b.Book).Include(b => b.Library).Include(b => b.Member).Include(b => b.Table).Include(b => b.Zone);
        //    return View(await miDbContext.ToListAsync());
        //}
        public async Task<IActionResult> Index(String busqueda)
        {
            var miDbContext = _context.Bookings.Include(b => b.Book).Include(b => b.Library).Include(b => b.Member).Include(b => b.Table).Include(b => b.Zone).OrderBy(b => b.BookingDate);
            try
            {
                if (busqueda != null)
                {

                    miDbContext = _context.Bookings.Include(b => b.Book).Include(b => b.Library).Include(b => b.Member).Include(b => b.Table).Include(b => b.Zone).Where(f => f.BookingDate >= DateTime.Parse(busqueda)).OrderBy(b => b.BookingDate);
                }
            }
            catch (FormatException e)
            {
                TempData["ErrorMessage"] = "El formato de fecha no es correcto.";
                return RedirectToAction(nameof(Index));
            }
            return View(await miDbContext.ToListAsync());
        }

        // GET: Bookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.Book)
                .Include(b => b.Library)
                .Include(b => b.Member)
                .Include(b => b.Table)
                .Include(b => b.Zone)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // GET: Bookings/Create
        public IActionResult Create()
        {
            ViewData["BookID"] = new SelectList(_context.Books, "ID", "Name");
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name");
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name");
            ViewData["TableID"] = new SelectList(_context.Tables, "ID", "NumberTable");
            ViewData["ZoneID"] = new SelectList(_context.Zones, "ID", "ID");
            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,BookingDate,MemberID,LibraryID,BookID,ZoneID,TableID")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                var bookLibrary = _context.Books.Where(b => b.ID == booking.BookID && b.LibraryID == booking.LibraryID).FirstOrDefault();

                if(bookLibrary == null)
                {
                    TempData["errorMessage"] = "El Libro no pertenece a esa Biblioteca";
                    return RedirectToAction(nameof(Create));
                }

                var zoneLibrary = _context.Zones.Where(z => z.ID == booking.ZoneID && z.LibraryID == booking.LibraryID).FirstOrDefault();

                if (zoneLibrary == null)
                {
                    TempData["errorMessage"] = "La Zona no pertenece a esa Biblioteca";
                    return RedirectToAction(nameof(Create));
                }

                var tableLibrary = _context.Tables.Include(t => t.Zone).Where(t => t.ID == booking.TableID && t.Zone.LibraryID == booking.LibraryID && t.ZoneID == booking.ZoneID).FirstOrDefault();

                if (tableLibrary == null)
                {
                    TempData["errorMessage"] = "La Mesa no pertenece a esa Biblioteca o a esa Zona";
                    return RedirectToAction(nameof(Create));
                }

                var bookingFecha = _context.Bookings.Where(b => b.BookingDate == booking.BookingDate &&
                (b.BookID == booking.BookID || b.TableID == booking.TableID)).FirstOrDefault();

                if(bookingFecha != null)
                {
                    TempData["errorMessage"] = "Ya está siendo usado el Libro o la Mesa en esa fecha";
                    return RedirectToAction(nameof(Create));
                }

                _context.Add(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookID"] = new SelectList(_context.Books, "ID", "Name", booking.BookID);
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", booking.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", booking.MemberID);
            ViewData["TableID"] = new SelectList(_context.Tables, "ID", "NumberTable", booking.TableID);
            ViewData["ZoneID"] = new SelectList(_context.Zones, "ID", "ID", booking.ZoneID);
            return View(booking);
        }

        // GET: Bookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            ViewData["BookID"] = new SelectList(_context.Books, "ID", "Name", booking.BookID);
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", booking.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", booking.MemberID);
            ViewData["TableID"] = new SelectList(_context.Tables, "ID", "NumberTable", booking.TableID);
            ViewData["ZoneID"] = new SelectList(_context.Zones, "ID", "Description", booking.ZoneID);
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BookingDate,MemberID,LibraryID,BookID,ZoneID,TableID")] Booking booking)
        {
            if (id != booking.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var bookLibrary = _context.Books.Where(b => b.ID == booking.BookID && b.LibraryID == booking.LibraryID).FirstOrDefault();

                    if (bookLibrary == null)
                    {
                        TempData["errorMessage"] = "El Libro no pertenece a esa Biblioteca";
                        return RedirectToAction(nameof(Edit));
                    }

                    var zoneLibrary = _context.Zones.Where(z => z.ID == booking.ZoneID && z.LibraryID == booking.LibraryID).FirstOrDefault();

                    if (zoneLibrary == null)
                    {
                        TempData["errorMessage"] = "La Zona no pertenece a esa Biblioteca";
                        return RedirectToAction(nameof(Edit));
                    }

                    var tableLibrary = _context.Tables.Include(t => t.Zone).Where(t => t.ID == booking.TableID && t.Zone.LibraryID == booking.LibraryID).FirstOrDefault();

                    if (tableLibrary == null)
                    {
                        TempData["errorMessage"] = "La Mesa no pertenece a esa Biblioteca o a esa Zona";
                        return RedirectToAction(nameof(Edit));
                    }

                    var bookingFecha = _context.Bookings.Where(b => b.BookingDate == booking.BookingDate &&
                    (b.BookID == booking.BookID || b.TableID == booking.TableID)).FirstOrDefault();

                    if (bookingFecha != null)
                    {
                        TempData["errorMessage"] = "Ya está siendo usado el Libro o la Mesa en esa fecha";
                        return RedirectToAction(nameof(Create));
                    }

                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            
            ViewData["BookID"] = new SelectList(_context.Books, "ID", "Name", booking.BookID);
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", booking.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", booking.MemberID);
            ViewData["TableID"] = new SelectList(_context.Tables, "ID", "NumberTable", booking.TableID);
            ViewData["ZoneID"] = new SelectList(_context.Zones, "ID", "Description", booking.ZoneID);
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.Book)
                .Include(b => b.Library)
                .Include(b => b.Member)
                .Include(b => b.Table)
                .Include(b => b.Zone)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var booking = await _context.Bookings.FindAsync(id);
            _context.Bookings.Remove(booking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(int id)
        {
            return _context.Bookings.Any(e => e.ID == id);
        }
    }
}
