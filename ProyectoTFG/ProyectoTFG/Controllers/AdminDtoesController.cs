﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Data;
using ProyectoTFG.Dtos;
using ProyectoTFG.Filters;
using ProyectoTFG.Models;

namespace ProyectoTFG.Controllers
{
    [ServiceFilter(typeof(VerificaSesion))]
    [ServiceFilter(typeof(VerificaAdmin))]
    public class AdminDtoesController : Controller
    {
        private readonly MiDbContext _context;

        public AdminDtoesController(MiDbContext context)
        {
            _context = context;
        }

        // GET: AdminDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AdminDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,Username,Password,Email,Active")] AdminDto adminDto)
        {
            if (ModelState.IsValid)
            {
                var rol = _context.Role.Where(r => r.Name.ToLower().Equals("admin")).FirstOrDefault();
                var existeAdmin = _context.UserAccounts.Where(u => u.Username == adminDto.Username).FirstOrDefault();

                if (existeAdmin == null)
                {
                    var user = new UserAccount();
                    user.Username = adminDto.Username;
                    user.Password = LoginController.Encriptar(adminDto.Password);
                    user.Email = adminDto.Email;
                    user.Active = true;
                    user.RoleID = rol.ID;
                    _context.UserAccounts.Add(user);
                    await _context.SaveChangesAsync();

                    var admin = new Admin();
                    admin.Name = adminDto.Name;
                    admin.Surname = adminDto.Surname;
                    admin.Phone = adminDto.Phone;
                    admin.UserAccountID = user.ID;
                    _context.Admins.Add(admin);
                    await _context.SaveChangesAsync();
                }
            }
            return RedirectToAction("Index", "Admins");
        }

    }
}
