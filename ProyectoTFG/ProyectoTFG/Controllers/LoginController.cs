﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Data;
using ProyectoTFG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ProyectoTFG.Controllers
{
    public class LoginController : Controller
    {
        private readonly MiDbContext _context;

        public LoginController(MiDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            MenusController.Menus = new List<Models.Menu>();

            return View();
        }

        public IActionResult Login(string username, string password)
        {
            string passwordEncrypted = Encriptar(password);

            var usuario = _context.UserAccounts.Include(u => u.Role).FirstOrDefault(u => u.Username.Equals(username) &&
            u.Password.Equals(passwordEncrypted));

            if (usuario != null)
            {
                HttpContext.Session.SetString("usuario", usuario.ID.ToString());
                HttpContext.Session.SetString("usuarioName", usuario.Username.ToString().Substring(0,1).ToUpper() + usuario.Username.ToString().Substring(1));
                HttpContext.Session.SetString("usuPassword", password);
                HttpContext.Session.SetString("rol", usuario.Role.Name.ToString());
                HttpContext.Session.SetString("rolId", usuario.Role.ID.ToString());

                //MenusController.Menus = _context.RoleHasMenu.Include(r => r.Menu).Where(r => r.RoleID.ToString() == HttpContext.Session.GetString("rolId"))
                //    .Select(r => r.Menu).ToList();
                
                string controlermn = "";
                string actionrmn = "";
                string labelmn = "";

                foreach (Menu menu in _context.RoleHasMenu.Include(m => m.Menu).Where(m => m.RoleID.ToString() == HttpContext.Session.GetString("rolId")).Select(m => m.Menu).ToList())
                {
                    controlermn += menu.Controller + ",";
                    actionrmn += menu.Action + ",";
                    labelmn += menu.Label + ",";
                }

                HttpContext.Session.SetString("controlermn", controlermn);
                HttpContext.Session.SetString("actionmn", actionrmn);
                HttpContext.Session.SetString("labelmn", labelmn);



                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index", "Login");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("usuario");
            HttpContext.Session.Remove("usuPassword");
            HttpContext.Session.Remove("rol");

            return RedirectToAction("Index", "Login");
        }

        public static string Encriptar(string clave)
        {
            byte[] encrypted = System.Text.Encoding.Unicode.GetBytes(clave);

            return Convert.ToBase64String(encrypted);
        }

        public static void SendEmail(string emailTo, string subject, string body)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            email.To.Add(new MailAddress(emailTo));
            email.From = new MailAddress("josemimusan@gmail.com");
            email.Subject = "subject";
            email.SubjectEncoding = System.Text.Encoding.UTF8;
            email.Body = body;
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("josemimusan@gmail.com", "altair1234$%");

            smtp.Send(email);
            smtp.Dispose();
        }
    }
}
