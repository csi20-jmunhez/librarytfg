﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Data;
using ProyectoTFG.Filters;
using ProyectoTFG.Models;

namespace ProyectoTFG.Controllers
{
    [ServiceFilter(typeof(VerificaSesion))]
    [ServiceFilter(typeof(VerificaAdmin))]
    public class SignLibrariesController : Controller
    {
        private readonly MiDbContext _context;

        public SignLibrariesController(MiDbContext context)
        {
            _context = context;
        }

        // GET: SignLibraries
        //public async Task<IActionResult> Index()
        //{
        //    var miDbContext = _context.SignLibrary.Include(s => s.Library).Include(s => s.Member);
        //    return View(await miDbContext.ToListAsync());
        //}

        public async Task<IActionResult> Index(String busqueda)
        {
            var miDbContext = _context.SignLibrary.Include(s => s.Library).Include(s => s.Member).OrderBy(s => s.Library.Name);
            try
            {
                if (busqueda != null)
                {

                    miDbContext = _context.SignLibrary.Include(s => s.Library).Include(s => s.Member).Where(n => n.Member.Name == busqueda).OrderBy(s => s.Library.Name);
                }
            }
            catch (FormatException e)
            {
                TempData["ErrorMessage"] = "El formato de texto no es correcto.";
                return RedirectToAction(nameof(Index));
            }
            return View(await miDbContext.ToListAsync());
        }

        // GET: SignLibraries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var signLibrary = await _context.SignLibrary
                .Include(s => s.Library)
                .Include(s => s.Member)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (signLibrary == null)
            {
                return NotFound();
            }

            return View(signLibrary);
        }

        // GET: SignLibraries/Create
        public IActionResult Create()
        {
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name");
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name");
            return View();
        }

        // POST: SignLibraries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,LibraryID,MemberID")] SignLibrary signLibrary)
        {
            if (ModelState.IsValid)
            {
                _context.Add(signLibrary);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", signLibrary.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", signLibrary.MemberID);
            return View(signLibrary);
        }

        // GET: SignLibraries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var signLibrary = await _context.SignLibrary.FindAsync(id);
            if (signLibrary == null)
            {
                return NotFound();
            }
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", signLibrary.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", signLibrary.MemberID);
            return View(signLibrary);
        }

        // POST: SignLibraries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LibraryID,MemberID")] SignLibrary signLibrary)
        {
            if (id != signLibrary.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(signLibrary);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SignLibraryExists(signLibrary.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LibraryID"] = new SelectList(_context.Libraries, "ID", "Name", signLibrary.LibraryID);
            ViewData["MemberID"] = new SelectList(_context.Members, "ID", "Name", signLibrary.MemberID);
            return View(signLibrary);
        }

        // GET: SignLibraries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var signLibrary = await _context.SignLibrary
                .Include(s => s.Library)
                .Include(s => s.Member)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (signLibrary == null)
            {
                return NotFound();
            }

            return View(signLibrary);
        }

        // POST: SignLibraries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var signLibrary = await _context.SignLibrary.FindAsync(id);
            _context.SignLibrary.Remove(signLibrary);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SignLibraryExists(int id)
        {
            return _context.SignLibrary.Any(e => e.ID == id);
        }
    }
}
