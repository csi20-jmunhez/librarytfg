﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Data;
using ProyectoTFG.Dtos;
using ProyectoTFG.Models;

namespace ProyectoTFG.Controllers
{
    public class MemberDtoesController : Controller
    {
        private readonly MiDbContext _context;

        public MemberDtoesController(MiDbContext context)
        {
            _context = context;
        }

        // GET: MemberDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MemberDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,Birthday,Dni,Address,City,PostalCode,Username,Password,Email,Active")] MemberDto memberDto)
        {
            if (ModelState.IsValid)
            {
                var rol = _context.Role.Where(r => r.Name.ToLower().Equals("member")).FirstOrDefault();
                var existeMember = _context.UserAccounts.Where(u => u.Username == memberDto.Username).FirstOrDefault();

                if (existeMember == null)
                {
                    var user = new UserAccount();
                    user.Username = memberDto.Username;
                    user.Password = LoginController.Encriptar(memberDto.Password);
                    user.Email = memberDto.Email;
                    user.Active = true;
                    user.RoleID = rol.ID;
                    _context.UserAccounts.Add(user);
                    await _context.SaveChangesAsync();

                    var member = new Member();
                    member.Name = memberDto.Name;
                    member.Surname = memberDto.Surname;
                    member.Phone = memberDto.Phone;
                    member.Address = memberDto.Address;
                    member.Birthday = memberDto.Birthday;
                    member.City = memberDto.City;
                    member.Dni = memberDto.Dni;
                    member.PostalCode = memberDto.PostalCode;
                    member.UserAccountID = user.ID;
                    _context.Members.Add(member);
                    await _context.SaveChangesAsync();
                }
            }
            return RedirectToAction("Index", "Members");
        }
    }
}
