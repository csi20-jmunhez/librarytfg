﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Dtos
{
    public class AdminDto
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The fiel {0} can´t be longer than 20 ")]
        public string Name { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "The fiel {0} can´t be longer than 80 ")]
        public string Surname { get; set; }
        [Required]
        [Phone(ErrorMessage = "The fiel {0} is not valid ")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "It can´t be empty")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "The number of characters must be between 5 and 10")]
        public string Username { get; set; }

        [Required(ErrorMessage = "It can´t be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(maximumLength: 50, ErrorMessage = "The number of characters must be between 0 and 50")]
        public string Email { get; set; }

        public bool Active { get; set; }
    }
}
