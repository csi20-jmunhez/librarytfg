﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProyectoTFG.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Filters
{
    public class VerificaSesion : IActionFilter
    {
        private readonly MiDbContext _context;

        public VerificaSesion(MiDbContext context)
        {
            _context = context;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {



        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccounts.Where(u => u.ID == Convert.ToUInt32(
                context.HttpContext.Session.GetString("usuario"))).FirstOrDefault();

            if (user == null)
            {
                context.Result = new RedirectToActionResult("Index", "Login", null);
            }
        }
    }
}
