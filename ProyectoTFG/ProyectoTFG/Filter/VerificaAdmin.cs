﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProyectoTFG.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Filters
{
    public class VerificaAdmin : IActionFilter
    {
        private readonly MiDbContext _context;

        public VerificaAdmin(MiDbContext context)
        {
            _context = context;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {



        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccounts.Where(u => u.ID == Convert.ToUInt32(
                context.HttpContext.Session.GetString("usuario"))).FirstOrDefault();

            var role = _context.Role.Where(r => r.ID.ToString() == context.HttpContext.Session.GetString("rolId")).FirstOrDefault();

            if (user == null)
            {
                context.Result = new RedirectToActionResult("Index", "Login", null);

            }else if (!role.Name.ToUpper().Equals("ADMIN"))
            {
                context.Result = new RedirectToActionResult("Index", "Home", null);
            }
        }
    }
}
