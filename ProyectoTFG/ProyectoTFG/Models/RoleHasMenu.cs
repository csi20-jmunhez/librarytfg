﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class RoleHasMenu
    {

        public int ID { get; set; }

        public int MenuID { get; set; }

        [ForeignKey("MenuID")]
        public Menu Menu { get; set; }

        public int RoleID { get; set; }

        [ForeignKey("RoleID")]
        public Role Role { get; set; }

    }
}
