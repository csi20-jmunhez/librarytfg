﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Member:Person
    {
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Required]
        [RegularExpression(@"[0-9]{8}[A-Z]{1}", ErrorMessage = "Document not valid")]
        public string Dni { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Address { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string City { get; set; }

        [Display(Name = "Postal_Code")]
        [Required]
        [StringLength(10, ErrorMessage = "The field {0} can &#39;t be longer than 10 characters")]
        public string PostalCode { get; set; }

        [InverseProperty("Member")]
        public List<SignLibrary> SignLibraryList { get; set; }

        [InverseProperty("Member")]
        public List<Booking> BookingList { get; set; }

    }
}
