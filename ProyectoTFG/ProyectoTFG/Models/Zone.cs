﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Zone
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Description { get; set; }

        public int LibraryID { get; set; }

        [ForeignKey("LibraryID")]
        public Library Library { get; set; }


        [InverseProperty("Zone")]
        public List<Booking> BookingList { get; set; }

        [InverseProperty("Zone")]
        public List<Table> TableList { get; set; }


    }
}
