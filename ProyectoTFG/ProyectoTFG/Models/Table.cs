﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Table
    {
        public int ID { get; set; }

        [Required]
        public string NumberTable { get; set; }

        public int ZoneID { get; set; }

        [ForeignKey("ZoneID")]
        public Zone Zone { get; set; }


        [InverseProperty("Table")]
        public List<Booking> BookingList { get; set; }
    }
}
