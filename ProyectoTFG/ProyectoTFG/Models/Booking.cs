﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Booking
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime BookingDate { get; set; }

        public int MemberID { get; set; }

        [ForeignKey("MemberID")]
        public Member Member { get; set; }

        public int LibraryID { get; set; }

        [ForeignKey("LibraryID")]
        public Library Library { get; set; }

        public int BookID { get; set; }

        [ForeignKey("BookID")]
        public Book Book { get; set; }

        public int ZoneID { get; set; }

        [ForeignKey("ZoneID")]
        public Zone Zone { get; set; }

        public int TableID { get; set; }

        [ForeignKey("TableID")]
        public Table Table { get; set; }
    }
}
