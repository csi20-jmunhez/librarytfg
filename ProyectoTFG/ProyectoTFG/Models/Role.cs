﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Role
    {

        public int ID { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        [StringLength(200, ErrorMessage = "The field {0} can't be longer than 200 characters")]
        public string Description { get; set; }

        [InverseProperty("Role")]
        public List<UserAccount> ListUserAccount { get; set; }

        [InverseProperty("Role")]
        public List<RoleHasMenu> ListRoleHasMenu { get; set; }
    }

}
