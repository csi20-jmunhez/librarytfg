﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Book
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string Author { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string Editorial { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string Gender { get; set; }

        [Required]
        public int Pages { get; set; }

        [Required]
        public double Rental_Price { get; set; }

        public int LibraryID { get; set; }

        [ForeignKey("LibraryID")]
        public Library Library { get; set; }


        [InverseProperty("Book")]
        public List<Booking> BookingList { get; set; }

    }
}
