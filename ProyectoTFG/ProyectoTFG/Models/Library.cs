﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Library
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Country { get; set; }
        
        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string City { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public string Address { get; set; }

        [Required]
        [Phone(ErrorMessage = "The field {0} is not valid")]
        public string Phone { get; set; }


        [InverseProperty("Library")]
        public List<Booking> BookingList { get; set; }


        [InverseProperty("Library")]
        public List<Book> BookList { get; set; }


        [InverseProperty("Library")]
        public List<SignLibrary> SignLibraryList { get; set; }

        [InverseProperty("Library")]
        public List<Zone> ZoneList { get; set; }
    }
}
