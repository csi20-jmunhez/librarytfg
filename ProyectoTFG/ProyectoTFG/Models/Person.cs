﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public abstract class Person
    {

        public int ID { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(20, ErrorMessage = "The field {0} can't be longer than 20 characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "The field {0} can't be longer than 80 characters")]
        public string Surname { get; set; }

        [Required]
        [Phone(ErrorMessage = "The field {0} is not valid")]
        public string Phone { get; set; }

        public int UserAccountID { get; set; }

        [ForeignKey("UserAccountID")]
        public UserAccount UserAccount { get; set; }

    }
}
