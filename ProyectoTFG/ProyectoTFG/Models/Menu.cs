﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class Menu
    {

        public int ID { get; set; }

        [Required]
        public string Controller { get; set; }

        [Required]
        public string Action { get; set; }

        [Required]
        public string Label { get; set; }

        [InverseProperty("Menu")]
        public List<RoleHasMenu> ListRoleHasMenu { get; set; }

        [NotMapped]
        public string Fullname
        {
            get => (Controller + " - " + Action);
        }

    }
}
