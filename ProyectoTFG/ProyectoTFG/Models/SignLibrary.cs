﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class SignLibrary
    {
        public int ID { get; set; }
        public int LibraryID { get; set; }

        [ForeignKey("LibraryID")]
        public Library Library { get; set; }

        public int MemberID { get; set; }

        [ForeignKey("MemberID")]
        public Member Member { get; set; }
    }
}
