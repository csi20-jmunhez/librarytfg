﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTFG.Models
{
    public class UserAccount
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "It can't be empty")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "the number of characters must be between 5 and 10")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 4, ErrorMessage = "the number of characters must be between 4 and 100 ")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(maximumLength: 50, ErrorMessage = "the number of characters must be between 0 and 50")]
        public string Email { get; set; }

        public bool Active { get; set; }

        public int RoleID { get; set; }

        [ForeignKey("RoleID")]
        public Role Role { get; set; }

    }
}
