﻿using Microsoft.EntityFrameworkCore;
using ProyectoTFG.Controllers;
using ProyectoTFG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProyectoTFG.Dtos;

namespace ProyectoTFG.Data
{
    public class MiDbContext : DbContext
    {

        public MiDbContext(DbContextOptions<MiDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Admin>().ToTable("Admin");
            //modelBuilder.Entity<Book>().ToTable("Book");
            //modelBuilder.Entity<Booking>().ToTable("Booking");
            //modelBuilder.Entity<Library>().ToTable("Library");
            //modelBuilder.Entity<Member>().ToTable("Member");
            //modelBuilder.Entity<Menu>().ToTable("Menu");
            //modelBuilder.Entity<Role>().ToTable("Role");
            //modelBuilder.Entity<RoleHasMenu>().ToTable("RoleHasMenu");
            //modelBuilder.Entity<SignLibrary>().ToTable("SignLibrary");
            //modelBuilder.Entity<Table>().ToTable("Table");
            //modelBuilder.Entity<UserAccount>().ToTable("UserAccount");
            //modelBuilder.Entity<Zone>().ToTable("Zone");

            //Menu
            modelBuilder.Entity<Menu>().HasData(
                new Menu
                {
                    ID = 1,
                    Controller = "UserAccounts",
                    Action = "Index",
                    Label = "UserAccounts"
                },
                new Menu
                {
                    ID = 2,
                    Controller = "Roles",
                    Action = "Index",
                    Label = "Roles"
                },
                new Menu
                {
                    ID = 3,
                    Controller = "RoleHasMenus",
                    Action = "Index",
                    Label = "RoleHasMenus"
                },
                new Menu
                {
                    ID = 4,
                    Controller = "Menus",
                    Action = "Index",
                    Label = "Menus"
                },
                new Menu
                {
                    ID = 5,
                    Controller = "Libraries",
                    Action = "Index",
                    Label = "Libraries"
                },
                new Menu
                {
                    ID = 6,
                    Controller = "Admins",
                    Action = "Index",
                    Label = "Admins"
                },
                new Menu
                {
                    ID = 7,
                    Controller = "Members",
                    Action = "Index",
                    Label = "Members"
                },
                new Menu
                {
                    ID = 8,
                    Controller = "Bookings",
                    Action = "Index",
                    Label = "Bookings"
                },
                new Menu
                {
                    ID = 9,
                    Controller = "SignLibraries",
                    Action = "Index",
                    Label = "SignLibraries"
                },
                new Menu
                {
                    ID = 10,
                    Controller = "Zones",
                    Action = "Index",
                    Label = "Zones"
                },
                new Menu
                {
                    ID = 11,
                    Controller = "Tables",
                    Action = "Index",
                    Label = "Tables"
                },
                new Menu
                {
                    ID = 12,
                    Controller = "Books",
                    Action = "Index",
                    Label = "Books"
                }
                );

            //Carga inicial de roles
            modelBuilder.Entity<Role>().HasData(
               new Role

               {
                   ID = 1,
                   Name = "Admin",
                   Description = "Rol de Admin"
               },
                 new Role

                 {
                     ID = 2,
                     Name = "Member",
                     Description = "Rol de Member"

                 });

            //Carga inicial de userAccount
            modelBuilder.Entity<UserAccount>().HasData(
                new UserAccount
                {
                    ID = 1,
                    Username = "admin",
                    Password = LoginController.Encriptar("Altair123$%"),
                    Email = "admin@email.es",
                    Active = false,
                    RoleID = 1
                },
                 new UserAccount
                 {
                     ID = 2,
                     Username = "member",
                     Password = LoginController.Encriptar("1234"),
                     Email = "member@email.es",
                     Active = true,
                     RoleID = 2
                 }
                );

            //Usuarios
            modelBuilder.Entity<Admin>().HasData(
                new Admin
                {
                    ID = 1,
                    Name = "Admin",
                    Surname = "Admin",
                    Phone = "123456789",
                    UserAccountID = 1

                });

            modelBuilder.Entity<Member>().HasData(
                new Member
                {
                    ID = 1,
                    Name = "Member",
                    Surname = "Member",
                    Phone = "123456789",
                    UserAccountID = 2,
                    Address = "Calle de ejemplo",
                    Birthday = DateTime.Now,
                    City = "Sevilla",
                    Dni = "12345678V",
                    PostalCode = "12345",
                });

            //Carga inicial de roleHasMenu
            modelBuilder.Entity<RoleHasMenu>().HasData(
                new RoleHasMenu
                {
                    ID = 1,
                    MenuID = 1,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 2,
                    MenuID = 2,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 3,
                    MenuID = 3,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 4,
                    MenuID = 4,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 5,
                    MenuID = 5,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 6,
                    MenuID = 6,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 7,
                    MenuID = 7,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 8,
                    MenuID = 8,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 9,
                    MenuID = 9,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 10,
                    MenuID = 10,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 11,
                    MenuID = 11,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 12,
                    MenuID = 12,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 13,
                    MenuID = 5,
                    RoleID = 2

                },
                new RoleHasMenu
                {
                    ID = 14,
                    MenuID = 12,
                    RoleID = 2
                }
            );

            //Library
            modelBuilder.Entity<Library>().HasData(
                new Library
                {
                    ID = 1,
                    Address = "Calle de ejemplo",
                    City = "Sevilla",
                    Country = "España",
                    Name = "Biblioteca",
                    Phone = "123456789",

                });

            //Carga SignLibrary
            modelBuilder.Entity<SignLibrary>().HasData(
                new SignLibrary
                {
                    ID = 1,
                    LibraryID = 1,
                    MemberID = 1
                });

            //Zone
            modelBuilder.Entity<Zone>().HasData(
                new Zone
                {
                    ID = 1,
                    Description = "Esto es de Prueba",
                    LibraryID = 1,
                },
                new Zone
                {
                    ID = 2,
                    Description = "Prueba2",
                    LibraryID = 1,
                }
                );

            //Table
            modelBuilder.Entity<Table>().HasData(
                new Table
                {
                    ID = 1,
                    NumberTable = "1",
                    ZoneID = 1,
                },
                new Table
                {
                    ID = 2,
                    NumberTable = "2",
                    ZoneID = 1,
                },
                new Table
                {
                    ID = 3,
                    NumberTable = "1",
                    ZoneID = 2,
                }
                );

            //Book
            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    ID = 1,
                    Editorial = "Editoria1",
                    Author = "Autor1",
                    Name = "Libro1",
                    Gender = "Action",
                    Pages = 120,
                    LibraryID = 1
                },
                new Book
                {
                    ID = 2,
                    Editorial = "Editoria1",
                    Author = "Autor2",
                    Name = "Libro2",
                    Gender = "Sci-Fiction",
                    Pages = 140,
                    LibraryID = 1
                },
                new Book
                {
                    ID = 3,
                    Editorial = "Editoria2",
                    Author = "Autor1",
                    Name = "Libro3",
                    Gender = "Tale",
                    Pages = 80,
                    LibraryID = 1
                });

            //Booking
            modelBuilder.Entity<Booking>().HasData(
                new Booking
                {
                    ID = 1,
                    BookingDate = new DateTime(),
                    BookID = 1,
                    LibraryID = 1,
                    MemberID = 1,
                    TableID = 3,
                    ZoneID = 2
                });

        }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Library> Libraries { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleHasMenu> RoleHasMenu { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<SignLibrary> SignLibrary { get; set; }

    }
}
